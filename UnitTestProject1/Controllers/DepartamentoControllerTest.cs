﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ECCI_IS_Lab01_WebApp.Controllers;
using System.Web.Mvc;
using System.Collections.Generic;
using ECCI_IS_Lab01_WebApp.Models;

namespace UnitTestProject1.Controllers
{
    [TestClass]
    public class DepartamentoControllerTest
    {
        [TestMethod]
        public void TestIndexNotNull()
        {
            DepartamentoController controller = new DepartamentoController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestCreateNotNull()
        {
            DepartamentoController controller = new DepartamentoController();
            ViewResult result = controller.Create() as ViewResult;
            Assert.IsNotNull(result);
        }
   
        [TestMethod]
        public void TestEditViewData()
        {
            DepartamentoController controller = new DepartamentoController();
            ViewResult result = controller.Edit(2) as ViewResult;
           Departamento departamento = (Departamento)result.ViewData.Model;
            Assert.AreEqual(2, departamento.Presupuesto);
        }

    }


}
