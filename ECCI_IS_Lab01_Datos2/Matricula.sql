﻿CREATE TABLE [dbo].[Matricula]
(
	[MatriculaID] INT IDENTITY(1, 1) NOT NULL,  
    [Nota] DECIMAL(3, 2) NULL, 
    [CursoID] INT NULL, 
    [EstudianteID] INT NULL, 
    CONSTRAINT [PK_Matricula] PRIMARY KEY ([MatriculaID]), 
      CONSTRAINT [FK_dbo.Matricula_dbo.Curso_CursoID]FOREIGN KEY ([CursoID])
    REFERENCES [dbo].[Curso] ([CursoID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Matricula_dbo.Estudiante_EstudianteID]FOREIGN KEY ([EstudianteID])
    REFERENCES [dbo].[Estudiante]([EstudianteID]) ON DELETE CASCADE
    
)
