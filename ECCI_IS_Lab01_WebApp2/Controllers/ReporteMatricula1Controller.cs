﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECCI_IS_Lab01_WebApp.Models;

namespace ECCI_IS_Lab01_WebApp.Controllers
{
    public class ReporteMatricula1Controller : Controller
    {
        private ECCI_IS_Lab01_DatosEntities db = new ECCI_IS_Lab01_DatosEntities();

        // GET: ReporteMatricula1
        public ActionResult Index()
        {
            return View(db.VistaMatricula1.ToList());
        }

        // GET: ReporteMatricula1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaMatricula1 vistaMatricula1 = db.VistaMatricula1.Find(id);
            if (vistaMatricula1 == null)
            {
                return HttpNotFound();
            }
            return View(vistaMatricula1);
        }

        // GET: ReporteMatricula1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ReporteMatricula1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MatriculaID,Nota,CursoID,EstudianteID")] VistaMatricula1 vistaMatricula1)
        {
            if (ModelState.IsValid)
            {
                db.VistaMatricula1.Add(vistaMatricula1);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vistaMatricula1);
        }

        // GET: ReporteMatricula1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaMatricula1 vistaMatricula1 = db.VistaMatricula1.Find(id);
            if (vistaMatricula1 == null)
            {
                return HttpNotFound();
            }
            return View(vistaMatricula1);
        }

        // POST: ReporteMatricula1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MatriculaID,Nota,CursoID,EstudianteID")] VistaMatricula1 vistaMatricula1)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vistaMatricula1).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vistaMatricula1);
        }

        // GET: ReporteMatricula1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VistaMatricula1 vistaMatricula1 = db.VistaMatricula1.Find(id);
            if (vistaMatricula1 == null)
            {
                return HttpNotFound();
            }
            return View(vistaMatricula1);
        }

        // POST: ReporteMatricula1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VistaMatricula1 vistaMatricula1 = db.VistaMatricula1.Find(id);
            db.VistaMatricula1.Remove(vistaMatricula1);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
